package com.hryshko.kafkametadata.controllers;

import com.hryshko.kafkametadata.entity.Topic;
import com.hryshko.kafkametadata.services.TopicsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TopicsRestController {
    private static Logger LOGGER = LoggerFactory.getLogger(TopicsController.class);

    private final TopicsService topicsService;

    public TopicsRestController(TopicsService topicsService) {
        this.topicsService = topicsService;
    }

    @GetMapping("/topics")
    //@RequestMapping(value = "/get", method = RequestMethod.GET)
    public List<Topic> getTopics () {
        List<Topic> topicsResponse = this.topicsService.getListOfTopics();
        return topicsResponse;
    }
}
