package com.hryshko.kafkametadata.controllers;

import com.hryshko.kafkametadata.services.TopicsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/topics")
public class TopicsController {

    private static Logger LOGGER = LoggerFactory.getLogger(TopicsController.class);

    private final TopicsService topicsService;

    public TopicsController(TopicsService topicsService) {
        this.topicsService = topicsService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getTopics(Model model) {
        model.addAttribute("topics", this.topicsService.getListOfTopics());
        return "kafka-topics";
    }
}
