package com.hryshko.kafkametadata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaMetadataApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaMetadataApplication.class, args);
    }

}
