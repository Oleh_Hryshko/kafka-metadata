package com.hryshko.kafkametadata.entity;


import javax.persistence.*;

@Entity
@Table(name="TOPIC")
public class Topic {
    @Id
    @Column(name="TOPIC_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int topicId;

    @Column(name="TOPIC_NAME")
    private String topicName;

    @Column(name="INTERNAL")
    private Boolean internal;

    @Column(name="NUMBER_OF_PARTITIONS")
    private Integer numberOfPartitions;

    @Column(name="REPLICATION_FACTOR")
    private Integer replicationFactor;

    public Topic(int topicId,
                 String topicName,
                 Boolean internal,
                 Integer numberOfPartitions,
                 Integer replicationFactor) {
        this.topicId = topicId;
        this.topicName = topicName;
        this.internal = internal;
        this.numberOfPartitions = numberOfPartitions;
        this.replicationFactor = replicationFactor;

    }

    public Topic() {

    }

    public int getTopicId() {
        return topicId;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public Boolean getInternal() {
        return internal;
    }

    public void setInternal(Boolean internal) {
        this.internal = internal;
    }

    public Integer getNumberOfPartitions() {
        return numberOfPartitions;
    }

    public void setNumberOfPartitions(Integer numberOfPartitions) {
        this.numberOfPartitions = numberOfPartitions;
    }

    public Integer getReplicationFactor() {
        return replicationFactor;
    }

    public void setReplicationFactor(Integer replicationFactor) {
        this.replicationFactor = replicationFactor;
    }

    @Override
    public String toString() {
        return "Topic{" +
                "topicId=" + topicId +
                ", topicName='" + topicName + '\'' +
                ", internal=" + internal +
                ", numberOfPartitions=" + numberOfPartitions +
                ", replicationFactor=" + replicationFactor +
                '}';
    }
}
