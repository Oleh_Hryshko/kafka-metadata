package com.hryshko.kafkametadata.services;

import com.hryshko.kafkametadata.entity.Topic;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.ListTopicsOptions;
import org.apache.kafka.clients.admin.TopicListing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class TopicsService {

    private static Logger LOGGER = LoggerFactory.getLogger(TopicsService.class);

    @Autowired
    public final AdminClient adminClient;

    public TopicsService(AdminClient adminClient) {
        this.adminClient = adminClient;
    }

    public List<Topic> getListOfTopics(){

        List<Topic> list = new ArrayList<>();
        AtomicInteger i = new AtomicInteger();

        try {
            Collection<TopicListing> listings = getTopicListing();

            List<String> topics = listings.stream().map(TopicListing::name)
                    .collect(Collectors.toList());

            DescribeTopicsResult result = adminClient.describeTopics(topics);

            result.allTopicNames().get().forEach((key, value) -> {
                list.add(new Topic(i.getAndIncrement(),
                        value.name(),
                        value.isInternal(),
                        value.partitions().size(),
                        value.partitions().get(0).replicas().size()));
            });

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            LOGGER.error( "Failed to get topic list" + e.getCause());
        }

        return list;
    }

    private Collection<TopicListing> getTopicListing() throws InterruptedException, ExecutionException {
        ListTopicsOptions options = new ListTopicsOptions();
        options.listInternal(true);
        return adminClient.listTopics(options).listings().get();
    }
}
